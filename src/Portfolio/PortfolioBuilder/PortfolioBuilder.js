import React from 'react';
import './PortfolioBuilder.css'
import Carousel from "../Carousel/Carousel";
import business from '../Carousel/images/business-2.jpg';
import cartpage from '../Carousel/images/cart-page.jpg';
import craze from '../Carousel/images/craze.jpg';
import cw from '../Carousel/images/CW-2.jpg';
import design from '../Carousel/images/design-7.jpg';
import designA from '../Carousel/images/design-13.jpg';
import flat from '../Carousel/images/flat.jpg';
import freedom from '../Carousel/images/freedom.jpg';
import icrowd from '../Carousel/images/icrowdme.jpg';
import cruise from '../Carousel/images/cruises.jpg';
import kappe from '../Carousel/images/kappe-homepage.jpg'
import joby from '../Carousel/images/joby (copy).jpg'
import intent from '../Carousel/images/intent (copy).jpg'
import indipixel from '../Carousel/images/indipixel.jpg';
import reform from '../Carousel/images/reform-2.jpg';
import {Route, Switch} from "react-router-dom";
import form from '../Carousel/images/form-desktop.jpg';
import fashion from '../Carousel/images/FashionPress (copy).jpg';
import cwPage from '../Carousel/images/TheyAllow-desktop.jpg';
import chat from '../Carousel/images/Chat.png';
import pokemon from '../Carousel/images/pokemon.png';
import todo from '../Carousel/images/todoPhoto.png';
import country from '../Carousel/images/country.png';
import countries from '../Carousel/images/Countries.png';
import orders from '../Carousel/images/orders.png';
import poker from '../Carousel/images/poker.png';
import todolist from '../Carousel/images/todo.png';
import quotes from '../Carousel/images/quotes.png';
import NavigationItem from "../../Component/NavigationItem/NavigationItem";

const simple =[
    {image: business, instruments: 'PerfectPixel', link: 'https://bayish.github.io/Business-page/'},
    {image: cartpage, instruments: 'PerfectPixel', link: 'https://bayish.github.io/cart-page/'},
    {image: craze, instruments: 'PerfectPixel', link: 'https://bayish.github.io/cwPage/'},
    {image: design, instruments: 'PerfectPixel', link: 'https://bayish.github.io/design-page/'},
    {image: flat, instruments: 'PerfectPixel', link: 'https://bayish.github.io/flat-page/'},
    {image: designA, instruments: 'PerfectPixel&DesktopFirst', link: 'https://bayish.github.io/desktopFirstPage/'},
    {image: form, instruments: 'PerfectPixel&DesktopFirst', link: 'https://bayish.github.io/Form-page/'},
    {image: icrowd, instruments: 'PerfectPixel', link: 'https://bayish.github.io/icrowWebPage/'},
    {image: reform, instruments: 'PerfectPixel', link: 'https://bayish.github.io/reform-page/'},
    {image: cw,  instruments: 'PerfectPixel&MobileFirst', link:   'https://bayish.github.io/secondWebpage/'},
    {image: cruise, instruments: 'DesktopFirst', link: 'https://bayish.github.io/cruise/'},
    {image: kappe, instruments: 'MobileFirst&Animation', link: 'https://bayish.github.io/kappePage/'},
];

const sass =[
    {image: intent, instruments: 'Mobile&desktop&SASS', link: 'https://bayish.github.io/intentPage/'},
    {image: indipixel, instruments: 'SASS&BEM', link: 'https://bayish.github.io/indixpage/'},
    {image: cwPage, instruments: 'SASS&BEM', link: 'https://bayish.github.io/cw-page/'},
];

const grid = [{image: fashion, instruments: 'SASS&BEM&Grid', link: 'https://bayish.github.io/fashionPage/'},];

const vanilla = [
    {image: todo, instruments: 'VanillaJS', link: 'https://bayish.github.io/todolistvanilla/'},
]

const bootstrap =[
    {image: freedom, instruments: 'Bootstrap', link: 'https://bayish.github.io/freedom-bootstrap-webpage/'},
    {image: joby, instruments: 'Mobile&desktop&Bootstrap', link: 'https://bayish.github.io/JobyPage/'},
]
const jquery = [
    {image: chat, instruments: 'Rest(AJAX)', link: 'https://github.com/Bayish/chatSimple'},
    {image: pokemon, instruments: 'Rest(AJAX)', link: 'https://bayish.github.io/pokemonInfo/'},
    {image: country, instruments: 'Rest(Axios)', link: 'https://bayish.github.io/country/'},
    {image: countries, instruments: 'Rest(Axios)', link: 'https://bayish.github.io/travelPlan/'},
];
const react =[
    {image: orders, instruments: 'react', link: 'https://github.com/Bayish/orderss'},
    {image: poker, instruments: 'react', link: 'https://github.com/Bayish/poker'},
    {image: todolist, instruments: 'react', link: 'https://github.com/Bayish/TodoList'},
    {image: quotes, instruments: 'react', link: 'https://github.com/Bayish/Quotes'},
];




const PortfolioBuilder = () => {
    return (
        <>
           <div className="portfolio">
               <div className="instruments">
                   <NavigationItem to='/portfolio' exact>All</NavigationItem>
                   <NavigationItem to='/portfolio/html-css'>HTML&CSS</NavigationItem>
                   <NavigationItem to='/portfolio/hc-sass'>SASS</NavigationItem>
                   <NavigationItem to='/portfolio/hc-grid'>Grid</NavigationItem>
                   <NavigationItem to='/portfolio/bootstrap'>Bootstrap</NavigationItem>
                   <NavigationItem to='/portfolio/hc-vanilla'>VanillaJs</NavigationItem>
                   <NavigationItem to='/portfolio/hc-jquery'>Jquery</NavigationItem>
                   <NavigationItem to='/portfolio/hc-react'>React</NavigationItem>
               </div>
               <div className="show">
                    <Switch>
                        <Route path={'/portfolio'} exact  render={() => (<Carousel content={[...simple, ...sass, ...bootstrap, ...grid, ...vanilla, ...jquery, ...react]}/>)}/>
                        <Route path={'/portfolio/html-css'} render={() => (<Carousel content={simple}/>)}/>
                        <Route path={'/portfolio/hc-sass'} render={() => (<Carousel content={sass}/>)}/>
                        <Route path={'/portfolio/bootstrap'} render={() => (<Carousel content={bootstrap}/>)}/>
                        <Route path={'/portfolio/hc-grid'} render={() => (<Carousel content={grid}/>)}/>
                        <Route path={'/portfolio/hc-vanilla'} render={() => (<Carousel content={vanilla}/>)}/>
                        <Route path={'/portfolio/hc-jquery'} render={() => (<Carousel content={jquery}/>)}/>
                        <Route path={'/portfolio/hc-react'} render={() => (<Carousel content={react}/>)}/>
                    </Switch>
               </div>
           </div>
        </>
    );
};

export default PortfolioBuilder;