import Slider from 'react-animated-slider';
import horizontalCss from 'react-animated-slider/build/horizontal.css';
import './Carousel.css'
import React from "react";


const Carousel = ({content}) => {
    return (
        <div className="html-css">
            <Slider classNames={horizontalCss}>
                {content.map((item, index) => (
                    <div
                        key={index}
                        className='sliderBack'
                        style={{
                            backgroundImage: `url('${item.image}')`,
                        }}
                    >
                        <div className="center">
                            <a href={item.link} target='_blank' rel="noreferrer">Open</a>
                        </div>
                        <h4 className="info-inst">{item.instruments}</h4>
                    </div>
                ))}
            </Slider>
        </div>

    );
};

export default Carousel;