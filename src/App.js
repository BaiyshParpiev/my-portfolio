import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './App.css';
import HomeBuilder from "./Home/HomeBuilder/HomeBuilder";
import PortfolioBuilder from "./Portfolio/PortfolioBuilder/PortfolioBuilder";
import ServicesBuilder from "./Services/ServicesBuilder/ServicesBuilder";
import ContactBuilder from "./Contact/ContactBuilder/ContactBuilder";
import Whatsapp from "./images/whatsapp-brands.svg";
import Facebook from "./images/facebook-brands.svg";
import Twitter from "./images/twitter-brands.svg";
import Instagram from "./images/instagram-square-brands.svg";
import Linkedin from "./images/linkedin-brands.svg";
import Bitbucket from "./images/bitbucket-brands.svg";
import gitHub from "./images/github-brands.svg";
import AboutBuilder from './About/AboutBuilder/AboutBuilder';
import Layout from "./Component/Layout/Layout";

const App = () => {

  return (
      <BrowserRouter>
        <div className="App">
          <Layout>
            <div className="block container">
              <Switch>
                <Route path="/" exact component={HomeBuilder}/>
                <Route path="/portfolio"  component={PortfolioBuilder}/>
                <Route path="/services" component={ServicesBuilder}/>
                <Route path="/about" component={AboutBuilder}/>
                <Route path="/contact" component={ContactBuilder}/>
              </Switch>
            </div>
          </Layout>
          <footer className="footer">
            <div className="container">
              <div className="footer__top row">
                <div className="footer__mail nowrap">
                  <a href="baiysh.parpiev@gmail.com" className="info">baiysh.parpiev@gmail.com</a>
                </div>
                <div className="footer__smedia nowrap">
                  <div className="social-media">
                    <ul className="social-media__list">
                      <li className="social-media__item"><a href="https://www.facebook.com/baiysh.parpiev" className="social-media__link"><img src={Facebook} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://www.instagram.com/bayish.parpiev" className=" social-media__link"><img src={Instagram} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://www.linkedin.com/in/bayish-parpiev-0b7534138/" className="social-media__link"><img src={Linkedin} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://t.me/frontenD96" className=" social-media__link"><img src={Whatsapp} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://twitter.com/BayishOfficial" className="social-media__link"><img src={Twitter} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://bitbucket.org/BaiyshParpiev/" className="social-media__link"><img src={Bitbucket} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://github.com/Bayish" className="social-media__link"><img src={gitHub} alt="link"/></a></li>
                    </ul>
                  </div>
                </div>
                <div className="footer__phone nowrap">
                  <a href="(+49) 1722962698" className="info">(+49) 1722962698</a>
                  <span className="footer__border"/>
                </div>
              </div>
              <div className="footer__bottom">
                <p className="web-info">Copyright(c) website name. <span className="web-info__text">Designed by: “it was made by me"</span> /
                  Images from: https://www.fontawesome.com/, www.photorack.net</p>
              </div>
            </div>
          </footer>
        </div>

      </BrowserRouter>
  )
}



export default App;
