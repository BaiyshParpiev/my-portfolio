import React from 'react';
import './About.css'
import me from '../../images/myPhoto.jpg';

const AboutBuilder = () => {

    return (
        <div className='about'>
            <div className='bio'>
                <div className="image-wrapper">
                    <a href="https://gmail.com" className="photo">
                        <img src={me} alt='me'/>
                        <h1>Baiysh Parpiev</h1>
                        <div className="glow-wrap">
                            <i className="glow"/>
                        </div>
                    </a>
                </div>
                <div className="info">
                    <h3>--Junior Frontend Developer</h3>
                    <p><i>"The two most important days in your life are the day you are born and the day you find out why."</i>–Mark Twain</p>
                </div>
            </div>
            <div className="skills">
                <div className="main">
                    <div>
                        <h3>HTML&CSS</h3>
                        <ul>
                            <li>SASS</li>
                            <li>SCSS</li>
                            <li>gulp</li>
                            <li>BEM</li>
                            <li>Mobile&Desktop</li>
                        </ul>
                    </div>
                    <div>
                        <h3>Language</h3>
                        <ul>
                            <li>Javascript ES6</li>
                        </ul>
                    </div>
                </div>
                <div className="central">
                    <div>
                        <h3>Frameworks</h3>
                        <ul>
                            <li>Bootstrap</li>
                            <li>Jquery</li>
                            <li>Material UI(beginner)</li>
                        </ul>
                    </div>
                    <div>
                        <h3>React</h3>
                        <ul>
                            <li>React-router-dom</li>
                            <li>Redux</li>
                        </ul>
                    </div>
                    <div>
                        <h3>Rest</h3>
                        <ul>
                            <li>Fetch</li>
                            <li>Axios</li>
                            <li>AJAX</li>
                        </ul>
                    </div>
                    <div>
                        <h3>Backend</h3>
                        <ul>
                            <li>Firebase</li>
                            <li>NodeJs(beginner)</li>
                        </ul>
                    </div>
                    <div>
                        <h3>Photoshop</h3>
                        <ul>
                            <li>Adobe</li>
                            <li>Figma</li>
                            <li>Perfect Pixel</li>
                        </ul>
                    </div>
                </div>
                <div className="instrumentsAbout">
                    <div>
                        <h3>Dev tools</h3>
                        <ul>
                            <li>WebStorm</li>
                            <li>Linux</li>
                            <li>Windows</li>
                            <li>VS Code</li>
                        </ul>
                    </div>
                    <div>
                        <h3>GIT</h3>
                        <ul>
                            <li>GitHub</li>
                            <li>BitBucket</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutBuilder;