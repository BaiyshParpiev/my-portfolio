import React from 'react';
import './Navigation.css'
import NavigationItem from "../NavigationItem/NavigationItem";

const NavigationItems = ({style}) => {
    return (
        <div className={`NavigationItems ${style? 'activeBlock' : 'noAktiv'}`}>
            <NavigationItem to='/' exact>Home</NavigationItem>
            <NavigationItem to="/portfolio">Portfolio</NavigationItem>
            <NavigationItem to="/services">Services</NavigationItem>
            <NavigationItem to="/about">About</NavigationItem>
            <NavigationItem to="/contact">Contact</NavigationItem>
        </div>
    );
};

export default NavigationItems;