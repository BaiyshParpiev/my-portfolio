import React, {useState} from 'react';
import './Toolbar.css'
import logo from "../../logo.svg";
import './Toolbar.css';
import NavigationItems from "../NavigationItems/NavigationItems";
import {useHistory} from "react-router-dom";

const Toolbar = () => {
    const [open, setOpen] = useState(false);
    const history = useHistory();

    return (
        <header className="App-header">
           <div className={`container  ${open? 'marginActive' : 'marginNotActive'}`}>
               <div className={`header Toolbar ${open? 'toolbarActive' : 'toolbarNotActive'}`}>
                   <a onClick={() => (history.replace('/'))} href='#' className={`header__logo  ${open? 'logoActive' : 'logoNotActive'}`}>
                       <img src={logo} className="App-logo" alt="logo" />
                   </a>
                   <div className='bars' onClick={() => setOpen(!open)}>
                       <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 448 512"
                            className="sc-jNMcJZ jkxheA" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                           <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
                       </svg>
                   </div>
                   <nav className="header__links">
                       <NavigationItems style={open ? 'inlineBlock' : ''}/>
                   </nav>
               </div>
           </div>
        </header>
    );
};

export default Toolbar;